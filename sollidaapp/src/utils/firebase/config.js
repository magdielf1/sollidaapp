import * as firebase from 'firebase';

var config = {
	apiKey: 'AIzaSyA2sLJsXug2PLSdf0dY3-GR79RlVKf51JI',
	authDomain: 'appsollida.firebaseapp.com',
	databaseURL: 'https://appsollida.firebaseio.com',
	projectId: 'appsollida',
	storageBucket: 'appsollida.appspot.com',
	messagingSenderId: '899501451624'
};

const firebaseApp = firebase.initializeApp(config);

export default firebaseApp;
