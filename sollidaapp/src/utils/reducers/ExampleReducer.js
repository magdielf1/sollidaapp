const INITIAL_STATE = {
    value: 10
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {  
        case 'setvalue':
            return {...state, value: action.payload}       
        default:
            return state;
    }
}   