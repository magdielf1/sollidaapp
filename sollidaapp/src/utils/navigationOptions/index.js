import React from 'react';
import TabIcon from "../../TabIcon";

import TitleHeader from "../../components/header/title";
import LeftHeader from "../../components/header/left";
import RightHeader from "../../components/header/right";

export const navigationOptionsStack = navigation => ({
  headerTitle: <TitleHeader navigation={navigation} />,
  headerStyle: {
    backgroundColor: "#26245de6"
  },
  headerLeft: <LeftHeader navigation={navigation}/>,
  headerRight: navigation.state.routes[navigation.state.index].routeName == 'Sollida' ? <RightHeader /> : null
});

export const navigationOptionsTab = (type, iconName, font) => ({
  tabBarIcon: ({ focused, tintColor }) =>
    focused ? (
      <TabIcon type={type} iconName={iconName} font={font} focused={true} />
    ) : (
      <TabIcon type={type} iconName={iconName} font={font} />
    ),
  tabBarOptions: {
    activeTintColor: "#7d7a7a",
    inactiveTintColor: "#b3b3b3"
  }
});
