export const setValue = (value) => {
    return(
        {
             type: 'setvalue',
             payload: value
        }                  
    )
}