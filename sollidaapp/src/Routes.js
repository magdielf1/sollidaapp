import {
	createStackNavigator,
	createBottomTabNavigator,
	createDrawerNavigator
} from 'react-navigation';

//navigationOptions
import {
	navigationOptionsStack,
	navigationOptionsTab
} from './utils/navigationOptions';

//components
import Home from './components/home';
import Invoice from './components/invoice';
import SendForm from './components/send/form';
import SendConfirm from './components/send/confirm';
import Login from './components/login';
import Register from './components/register';
import Recovery from './components/recovery';

//Drawer
import SideMenu from './components/sideMenu';
import { Dimensions } from 'react-native';

const RoutesTab = createBottomTabNavigator({
	Sollida: {
		screen: Home,
		navigationOptions: navigationOptionsTab(0, 'wallet', false)
	},
	Request: {
		screen: Invoice,
		navigationOptions: navigationOptionsTab(0, 'download', true)
	},
	Send: {
		screen: SendForm,
		navigationOptions: navigationOptionsTab(0, 'upload', true)
	},
	Exchange: {
		screen: Home,
		navigationOptions: navigationOptionsTab(0, 'repeat', true)
	}
});

RoutesTab.navigationOptions = ({ navigation }) =>
	navigationOptionsStack(navigation);

const StackNavgation = createStackNavigator({
	Stack: {
		screen: RoutesTab
	}
});

const DrawerNavigation = createDrawerNavigator(
	{
		Root: {
			screen: StackNavgation
		}
	},
	{
		contentComponent: SideMenu,
		drawerWidth: Dimensions.get('window').width * 0.8
	}
);

DrawerNavigation.navigationOptions = {
	header: null
};

export const Routes = createStackNavigator(
	{
		Login: {
			screen: Login
		},
		Register: {
			screen: Register
		},
		Recovery: {
			screen: Recovery
		},
		SendConfirm: {
			screen: SendConfirm
		},
		DrawerNavigation: {
			screen: DrawerNavigation
		}
	},
	{
		initialRouteName: 'Login',
		headerMode: 'none',
		navigationOptions: {
			headerVisible: false
		}
	}
);
