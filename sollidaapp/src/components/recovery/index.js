import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	ImageBackground,
	TouchableOpacity
} from 'react-native';
import {
	Container,
	Content,
	Row,
	Col,
	Thumbnail,
	Button,
	Input,
	Item
} from 'native-base';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import { connect } from 'react-redux';
import { setValue } from '../../utils/actions/example';

class Recovery extends React.Component {
	render() {
		return (
			<React.Fragment>
				<Container style={{ backgroundColor: 'rgb(76, 188, 236)' }}>
					<ImageBackground
						source={require('../../../assets/splashUser.png')}
						style={{ width: '100%', height: '100%' }}
					>
						<Content>
							<View>
								<Row
									style={{
										width: '100%',
										padding: '10%',
										marginTop: '20%',
										justifyContent: 'center',
										alignItems: 'center'
									}}
								>
									<Thumbnail
										square
										large
										style={{ width: 100 }}
										source={require('../../../assets/iconApp.png')}
									/>
								</Row>
								<Row
									style={{
										width: '100%',
										padding: '2%',
										paddingLeft: '5%',
										paddingRight: '5%'
									}}
								>
									<Item
										style={{
											width: '100%',
											backgroundColor: '#eff5fe8f',
											borderRadius: 80
										}}
									>
										<Entypo
											style={{ color: '#ffffffd1', top: 1, left: 5 }}
											name="email"
											size={20}
										/>
										<Input
											placeholder="Email"
											style={{
												marginLeft: 10,
												padding: '5%'
											}}
										/>
									</Item>
								</Row>

								<Row
									style={{
										width: '100%',
										padding: '2%',
										paddingRight: '5%',
										paddingLeft: '5%'
									}}
								>
									<Button
										rounded
										style={{
											width: '100%',
											justifyContent: 'center',
											alignItems: 'center',
											backgroundColor: '#367bf5'
										}}
									>
										<Text
											style={{
												color: 'white',
												fontWeight: 'bold',
												color: '#f9f8f8'
											}}
										>
											Recovery
										</Text>
									</Button>
								</Row>

								<Row
									style={{
										width: '100%',
										justifyContent: 'center',
										alignItems: 'center',
										paddingRight: '5%',
										paddingLeft: '5%'
									}}
								>
									<TouchableOpacity
										onPress={() => {
											this.props.navigation.navigate('Login');
										}}
									>
										<Text
											style={{
												marginTop: '6%',
												fontWeight: 'bold',
												color: '#f9f8f8'
											}}
										>
											Already user? Login!
										</Text>
									</TouchableOpacity>
								</Row>
							</View>
						</Content>
					</ImageBackground>
				</Container>
			</React.Fragment>
		);
	}
}

const styles = StyleSheet.create({
	viewAlign: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttons: {
		alignItems: 'center'
	}
});

const mapStateToProps = state => ({
	value: state.ExampleReducer.value
});

const mapActionToProps = {
	setValue
};

export default connect(
	mapStateToProps,
	mapActionToProps
)(Recovery);
