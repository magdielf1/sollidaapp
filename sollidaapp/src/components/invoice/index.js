import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Slider,
  Dimensions,
  ScrollView
} from "react-native";
import { Container, Content, Row } from "native-base";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { QRCode } from "react-native-custom-qr-codes";
import { connect } from "react-redux";
import { setValue } from "../../utils/actions/example";

class Invoice extends React.Component {
  render() {
    return (
      <Container>
        <ImageBackground
          source={require("../../../assets/bg.png")}
          style={{ width: "100%", height: "100%" }}
        >
          <Content style={{ paddingHorizontal: 25 }}>
            <Row style={styles.unitSLD}>
              <Text style={styles.textunitSLD}>1 SLD = $ 547.53 USD</Text>
            </Row>

            <Row style={styles.menu}>
              <MaterialCommunityIcons
                style={{ color: "white" }}
                name="currency-eth"
                size={30}
              />
              <Text style={styles.textSLD}>0</Text>
              <Text style={styles.textSLD}>SLD</Text>
            </Row>
            <Row style={{ width: "100%", marginVertical: 5 }}>
              <Slider
                minimumValue={1}
                maximumValue={1000000}
                minimumTrackTintColor={"#41b1ea"}
                maximumTrackTintColor={"#51b3e5"}
                thumbTintColor={"#51b3e5"}
                onValueChange={data => {
                  console.log(data);
                }}
                style={{ width: "100%" }}
              />
            </Row>
            <Row style={styles.menu}>
              <MaterialCommunityIcons
                style={{ color: "white" }}
                name="currency-usd"
                size={16}
              />
              <Text style={styles.textUSD}>0</Text>
              <Text style={styles.textUSD}>USD</Text>
            </Row>
            <Row style={{ justifyContent: "center", marginVertical: 30 }}>
              <QRCode
                content="https://sollida.com/"
                size={Dimensions.get("window").width - 160}
                backgroundImage={require("../../../assets/bg.png")}
              />
            </Row>
            <Row style={{ justifyContent: "center" }}>
              <Text style={styles.textToken}>Waiting for payment...</Text>
            </Row>
            <Row style={{ justifyContent: "center" }}>
              <Text style={styles.textToken}>
                jrvboerivbeoçibçrobrçoiebçroibefçoibvçouv
              </Text>
            </Row>
          </Content>
        </ImageBackground>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  unitSLD: {
    marginTop: 25,
    marginBottom: 55,
    justifyContent: "center"
  },
  textunitSLD: {
    color: "white",
    fontSize: 20,
    fontWeight: "400"
  },
  menu: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 15
  },
  textSLD: {
    fontWeight: "500",
    color: "white",
    fontSize: 19
  },
  slider: {
    marginVertical: 10
  },
  textUSD: {
    fontWeight: "100",
    color: "white"
  },
  textToken: {
    fontWeight: "100",
    color: "white",
    fontSize: 12
  }
});

const mapStateToProps = state => ({
  value: state.ExampleReducer.value
});

const mapActionToProps = {
  setValue
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(Invoice);
