import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	ImageBackground,
	TouchableOpacity
} from 'react-native';
import {
	Container,
	Content,
	Row,
	Col,
	Thumbnail,
	Button,
	Input,
	Item,
	Spinner,
	Toast,
	Root
} from 'native-base';
import { StackNavigator } from 'react-navigation';
import { Font } from 'expo';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import firebaseApp from '../../utils/firebase/config';
import { connect } from 'react-redux';
import { setValue } from '../../utils/actions/example';

class Register extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			email: 'legendrotemp@gmail.com',
			username: 'magdielf1',
			password: 'diel1945',
			loading: false
		};
	}

	updateUser = user => {
		let _this = this;
		var updateDisplay = firebaseApp.auth().currentUser;
		updateDisplay
			.updateProfile({
				displayName: user
			})
			.then(function(user) {
				console.log(user);
				_this.goToWallet();
			})
			.catch(function(error) {
				_this.setState({ loading: false });
			});
	};
	goToWallet() {
		this.props.navigation.navigate('Login');
	}
	registerNewUser() {
		let email = this.state.email,
			password = this.state.password,
			username = this.state.username;
		_this = this;
		if (username === null) {
			Toast.show({
				text: 'Fill username form!',
				buttonText: 'close',
				duration: 3000
			});
			return;
		}
		if (email === null) {
			Toast.show({
				text: 'Fill email form!',
				buttonText: 'close',
				duration: 3000
			});
			return;
		}
		if (password === null) {
			Toast.show({
				text: 'Fill password form!',
				buttonText: 'close',
				duration: 3000
			});
			return;
		}
		this.setState({ loading: true });
		firebaseApp
			.auth()
			.createUserWithEmailAndPassword(email, password)
			.then(user => {
				_this.updateUser(username);
			})
			.catch(error => {
				//tratamos o erro
				// var errorMessage = error.message;
				_this.setState({ loading: false });
				var errorCode = error.code;
				Toast.show({
					text: errorCode,
					buttonText: 'Okay',
					duration: 3000
				});
			});
	}
	async componentWillMount() {
		await Font.loadAsync({
			Roboto: require('native-base/Fonts/Roboto.ttf'),
			Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf')
		});
	}
	render() {
		return (
			<Root>
				<Container style={{ backgroundColor: 'rgb(76, 188, 236)' }}>
					<ImageBackground
						source={require('../../../assets/splashUser.png')}
						style={{ width: '100%', height: '100%' }}
					>
						<Content>
							<View>
								<Row
									style={{
										width: '100%',
										marginTop: '20%',
										justifyContent: 'center',
										alignItems: 'center'
									}}
								>
									<Thumbnail
										square
										large
										style={{ width: 100 }}
										source={require('../../../assets/iconApp.png')}
									/>
								</Row>
								<Row
									style={{
										width: '100%',
										padding: '2%',
										paddingLeft: '5%',
										paddingRight: '5%'
									}}
								>
									<Item
										style={{
											width: '100%',
											backgroundColor: '#eff5fe8f',
											borderRadius: 80
										}}
									>
										<Entypo
											style={{ color: '#ffffffd1', top: 1, left: 5 }}
											name="user"
											size={20}
										/>
										<Input
											placeholder="Username"
											onChangeText={text => this.setState({ username: text })}
											style={{
												marginLeft: 10,
												padding: '5%'
											}}
										/>
									</Item>
								</Row>
								<Row
									style={{
										width: '100%',
										padding: '2%',
										paddingLeft: '5%',
										paddingRight: '5%'
									}}
								>
									<Item
										style={{
											width: '100%',
											backgroundColor: '#eff5fe8f',
											borderRadius: 80
										}}
									>
										<Entypo
											style={{ color: '#ffffffd1', top: 1, left: 5 }}
											name="email"
											size={20}
										/>
										<Input
											placeholder="Email"
											onChangeText={text => this.setState({ email: text })}
											style={{
												marginLeft: 10,
												padding: '5%'
											}}
										/>
									</Item>
								</Row>
								<Row
									style={{
										width: '100%',
										padding: '2%',
										paddingRight: '5%',
										paddingLeft: '5%'
									}}
								>
									<Item
										style={{
											width: '100%',
											backgroundColor: '#eff5fe8f',
											borderRadius: 80
										}}
									>
										<Entypo
											style={{ color: '#ffffffd1', top: 1, left: 5 }}
											name="lock"
											size={20}
										/>
										<Input
											placeholder="Password"
											onChangeText={text => this.setState({ password: text })}
											style={{
												marginLeft: 10,
												padding: '5%'
											}}
										/>
									</Item>
								</Row>
								<Row
									onPress={() => this.registerNewUser()}
									style={{
										width: '100%',
										padding: '2%',
										paddingRight: '5%',
										paddingLeft: '5%'
									}}
								>
									<Button
										rounded
										disabled={this.state.loading}
										style={{
											width: '100%',
											justifyContent: 'center',
											alignItems: 'center',
											backgroundColor: this.state.loading
												? '#a6a6a6'
												: '#367bf5'
										}}
									>
										{this.state.loading ? (
											<Spinner color="white" />
										) : (
											<Text
												style={{
													color: 'white',
													fontWeight: 'bold',
													color: '#f9f8f8'
												}}
											>
												Register
											</Text>
										)}
									</Button>
								</Row>
								<Row
									style={{
										width: '100%',
										justifyContent: 'center',
										alignItems: 'center',
										paddingRight: '5%',
										paddingLeft: '5%'
									}}
								>
									<TouchableOpacity
										onPress={() => {
											this.props.navigation.navigate('Login');
										}}
									>
										<Text
											style={{
												marginTop: '6%',
												fontWeight: 'bold',
												color: '#f9f8f8'
											}}
										>
											Already user? Login!
										</Text>
									</TouchableOpacity>
								</Row>
							</View>
						</Content>
					</ImageBackground>
				</Container>
			</Root>
		);
	}
}

const styles = StyleSheet.create({
	viewAlign: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttons: {
		alignItems: 'center'
	}
});

const mapStateToProps = state => ({
	value: state.ExampleReducer.value
});

const mapActionToProps = {
	setValue
};

export default connect(
	mapStateToProps,
	mapActionToProps
)(Register);
