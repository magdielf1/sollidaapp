import React from "react";
import { StyleSheet, Text } from "react-native";
import { Row, Thumbnail, Left, Right, Body, ListItem } from "native-base";
import Entypo from "react-native-vector-icons/Entypo";
import { connect } from "react-redux";
import { setValue } from "../../../utils/actions/example";

import { LinearGradient } from "expo";

class myListUser extends React.Component {
  render() {
    return (
      <React.Fragment>
        <ListItem thumbnail>
          <Left>
            <Thumbnail
              source={{
                uri: "https://image.flaticon.com/icons/png/512/149/149071.png"
              }}
            />
          </Left>
          <Body>
            <Text>
              <Text style={{ fontWeight: "bold" }}>{this.props.userName}</Text>{" "}
              pagou a você
            </Text>
            <Text style={{ color: "#989ea5" }}>{this.props.text}</Text>
            <Row>
              <Left>
                <Text style={{ width: 140 }}>
                  <Text style={{ color: "#0fc35e", fontWeight: "bold" }}>
                    {" "}
                    {this.props.value} SLD
                  </Text>
                  <Text style={{ color: "#b0b0b0" }}>
                    {" "}
                    |{" "}
                    <Entypo
                      style={{ color: "#b0b0b0", top: 1 }}
                      name="time-slot"
                      size={17}
                    />
                    {this.props.time}
                  </Text>
                </Text>
              </Left>
              <Right>
                <Text style={{ width: 70, color: "#b0b0b0" }}>
                  <Entypo
                    style={{ color: "#b0b0b0", top: 1 }}
                    name="chat"
                    size={17}
                  />
                  {this.props.comments}{" "}
                  <Entypo
                    style={{ color: "#b0b0b0", top: 1 }}
                    name="heart"
                    size={17}
                  />
                  {this.props.heart}
                </Text>
              </Right>
            </Row>
          </Body>
        </ListItem>
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  viewAlign: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

const mapStateToProps = state => ({
  value: state.ExampleReducer.value
});

const mapActionToProps = {
  setValue
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(myListUser);
