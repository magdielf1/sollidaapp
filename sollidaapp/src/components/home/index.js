import React from "react";
import SubHeader from "./subheader";
import HistoryHome from "./historyHome";
import { connect } from "react-redux";
import { setValue } from "../../utils/actions/example";
import { Button } from "react-native";
class Home extends React.Component {
  render() {
    return (
      <React.Fragment>
        <SubHeader />
        <HistoryHome />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  value: state.ExampleReducer.value
});

const mapActionToProps = {
  setValue
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(Home);
