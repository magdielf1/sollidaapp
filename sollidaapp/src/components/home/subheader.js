import React from "react";
import { StyleSheet, Text, View, ImageBackground } from "react-native";
import { Container, Row, Col, Thumbnail, Button } from "native-base";
import Entypo from "react-native-vector-icons/Entypo";
import Feather from "react-native-vector-icons/Feather";
import { connect } from "react-redux";
import { setValue } from "../../utils/actions/example";

class SubHeader extends React.Component {
  render() {
    return (
      <React.Fragment>
        <View style={styles.viewAlign}>
          <ImageBackground
            source={require("../../../assets/bg.png")}
            style={{ width: "100%", height: "100%" }}
          >
            <View style={styles.viewAlign}>
              <Thumbnail
                square
                small
                style={{ marginTop: 10 }}
                source={require("../../../assets/fav2.png")}
              />

              <Row>
                <Entypo
                  style={{ color: "white", top: 1, left: -20 }}
                  name="list"
                  size={35}
                />
                <Text style={{ fontSize: 27, color: "white" }}>
                  0.000000 SLD
                </Text>
              </Row>

              <Row>
                <Col>
                  <Button
                    info
                    onPress={() => false}
                    style={{
                      marginHorizontal: "5%",
                      padding: "10%"
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 20,
                        color: "white",
                        width: "100%",
                        textAlign: "center"
                      }}
                    >
                      <Feather
                        style={{ color: "white", top: 3 }}
                        name="download"
                        size={35}
                      />{" "}
                      Requests{" "}
                    </Text>
                  </Button>
                </Col>
                <Col>
                  <Button
                    info
                    style={{
                      marginHorizontal: "5%",
                      padding: "10%"
                    }}
                    onPress={() => {
                      this.props.setValue(this.props.value + 1); // não faça isso em casa                     
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 20,
                        color: "white",
                        width: "100%",
                        textAlign: "center"
                      }}
                    >
                      <Feather
                        style={{ color: "white", top: 3 }}
                        name="upload"
                        size={35}
                      />{" "}
                      Send{" "}
                    </Text>
                  </Button>
                </Col>
              </Row>
            </View>
          </ImageBackground>
        </View>
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  viewAlign: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  buttons: {
    alignItems: "center"
  }
});

const mapStateToProps = state => ({
  value: state.ExampleReducer.value
});

const mapActionToProps = {
  setValue
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(SubHeader);
