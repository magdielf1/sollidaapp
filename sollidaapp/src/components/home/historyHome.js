import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Container } from 'native-base';
import MyListUser from './historyHomeComp/myList';
import { connect } from 'react-redux';
import { setValue } from '../../utils/actions/example';

class HistoryHome extends React.Component {
	render() {
		return (
			<React.Fragment>
				<View style={{ flex: 1 }}>
					<ScrollView contentContainerStyle={{ flexGrow: 1 }}>
						<Text
							style={{
								backgroundColor: '#ccc',
								width: '100%',
								paddingTop: '2%',
								paddingBottom: '2%',
								paddingLeft: '3%'
							}}
						>
							Oct 24, 2018
						</Text>

						<MyListUser
							userName="@valadares"
							value="75"
							text="Minha primeira compra com SLD, isso é louco!"
							time="16:22"
							comments="8"
							heart="2"
						/>
						<MyListUser
							userName="@samuka"
							value="2.3k"
							text="Aquela grana que eu tava devendo a você!"
							time="12:22"
							comments="81"
							heart="2"
						/>
						<MyListUser
							userName="@arthur"
							value="275"
							text="Minha parte na vaquinha da nossa festa!"
							time="14:22"
							comments="2"
							heart="32"
						/>
						<MyListUser
							userName="@valetria"
							value="955"
							text="Valor da mensalidade da casa!"
							time="09:22"
							comments="3"
							heart="2"
						/>
					</ScrollView>
				</View>
			</React.Fragment>
		);
	}
}

const mapStateToProps = state => ({
	value: state.ExampleReducer.value
});

const mapActionToProps = {
	setValue
};

export default connect(
	mapStateToProps,
	mapActionToProps
)(HistoryHome);
