import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import Entypo from "react-native-vector-icons/Entypo";

export default class LeftHeader extends Component {
  render() {
    return (
      <View style={{ paddingHorizontal: 10 }}>
        <Entypo
          onPress={() => this.props.navigation.openDrawer()}
          style={{ color: "white" }}
          name={"menu"}
          size={35}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({});
