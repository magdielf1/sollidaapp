import React, { Component } from "react";
import { View } from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

export default class RightHeader extends Component {
  render() {
    return (
      <View style={{ paddingHorizontal: 10 }}>
        <MaterialIcons style={{ color: "white" }} name={"keyboard-arrow-down"} size={35} />
      </View>
    );
  }
}
