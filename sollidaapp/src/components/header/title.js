import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Text } from 'native-base';

export default class TitleHeader extends Component {
  render() {
    let { navigation } = this.props;
    console.log(JSON.stringify(navigation));
    let routeName = navigation.state.routes[navigation.state.index].routeName.toUpperCase();
    return (
      <Text style={styles.title}> {routeName}</Text>
    );
  }
}
 
const styles = StyleSheet.create({
  title: {
    color: 'white',
    fontWeight: '500'
  }
});