import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	ImageBackground,
	TouchableOpacity
} from 'react-native';
import {
	Container,
	Content,
	Row,
	Col,
	Thumbnail,
	Button,
	Input,
	Item,
	Toast,
	Root,
	Spinner
} from 'native-base';
import { Font } from 'expo';
import firebaseApp from '../../utils/firebase/config';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import { connect } from 'react-redux';
import { setValue } from '../../utils/actions/example';

class Login extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: true,
			email: '',
			password: ''
		};
	}
	userLogin() {
		let email = this.state.email,
			password = this.state.password;
		firebaseApp
			.auth()
			.signInWithEmailAndPassword(email, password)
			.then(() => {
				Toast.show({
					text: 'Welcome to Sollida Network!',
					buttonText: 'close',
					duration: 3000
				});
				return;
			})
			.catch(function(error) {
				// var errorMessage = error.message;
				Toast.show({
					text: error.message,
					buttonText: 'close',
					duration: 3000
				});
				return;
			});
	}
	goToWallet() {
		this.props.navigation.navigate('DrawerNavigation');
	}
	async componentWillMount() {
		let _this = this;
		//detecta se o usuário mudou de status para ''autenticado'' se estiver com status ''não antenticado
		//o usuário será redirecionado para a página de login
		firebaseApp.auth().onAuthStateChanged(function(user) {
			if (user) {
				_this.goToWallet();
			} else {
				_this.setState({ loading: false });
			}
		});
		await Font.loadAsync({
			Roboto: require('native-base/Fonts/Roboto.ttf'),
			Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf')
		});
	}

	render() {
		return (
			<Root>
				<Container style={{ backgroundColor: 'rgb(76, 188, 236)' }}>
					<ImageBackground
						source={require('../../../assets/splashUser.png')}
						style={{ width: '100%', height: '100%' }}
					>
						<Content>
							<View>
								{this.state.loading ? (
									<Row
										style={{
											width: '100%',
											padding: '10%',
											marginTop: '40%',
											justifyContent: 'center',
											alignItems: 'center'
										}}
									>
										<Spinner color="white" />
									</Row>
								) : (
									<React.Fragment>
										<Row
											style={{
												width: '100%',
												padding: '10%',
												marginTop: '20%',
												justifyContent: 'center',
												alignItems: 'center'
											}}
										>
											<Thumbnail
												square
												large
												style={{ width: 100 }}
												source={require('../../../assets/iconApp.png')}
											/>
										</Row>

										<Row
											style={{
												width: '100%',
												padding: '2%',
												paddingLeft: '5%',
												paddingRight: '5%'
											}}
										>
											<Item
												style={{
													width: '100%',
													backgroundColor: '#eff5fe8f',
													borderRadius: 80
												}}
											>
												<Entypo
													style={{ color: '#ffffffd1', top: 1, left: 5 }}
													name="email"
													size={20}
												/>
												<Input
													placeholder="Email"
													onChangeText={text => this.setState({ email: text })}
													style={{
														marginLeft: 10,
														padding: '5%'
													}}
												/>
											</Item>
										</Row>

										<Row
											style={{
												width: '100%',
												padding: '2%',
												paddingRight: '5%',
												paddingLeft: '5%'
											}}
										>
											<Item
												style={{
													width: '100%',
													backgroundColor: '#eff5fe8f',
													borderRadius: 80
												}}
											>
												<Entypo
													style={{ color: '#ffffffd1', top: 1, left: 5 }}
													name="lock"
													size={20}
												/>
												<Input
													placeholder="Password"
													onChangeText={text =>
														this.setState({ password: text })
													}
													style={{
														marginLeft: 10,
														padding: '5%'
													}}
												/>
											</Item>
										</Row>
										<Row
											onPress={() => this.userLogin()}
											style={{
												width: '100%',
												padding: '2%',
												paddingRight: '5%',
												paddingLeft: '5%'
											}}
										>
											<Button
												rounded
												style={{
													width: '100%',
													justifyContent: 'center',
													alignItems: 'center',
													backgroundColor: '#367bf5'
												}}
											>
												<Text
													style={{
														color: 'white',
														fontWeight: 'bold',
														color: '#f9f8f8'
													}}
												>
													Login
												</Text>
											</Button>
										</Row>
										<Row
											style={{
												width: '100%',
												justifyContent: 'center',
												alignItems: 'center',
												paddingRight: '5%',
												paddingLeft: '5%'
											}}
										>
											<TouchableOpacity
												onPress={() => {
													this.props.navigation.navigate('Recovery');
												}}
											>
												<Text
													style={{
														marginTop: '6%',
														fontWeight: 'bold',
														color: '#f9f8f8'
													}}
												>
													Forgot password
												</Text>
											</TouchableOpacity>
										</Row>
										<Row
											style={{
												width: '100%',
												justifyContent: 'center',
												alignItems: 'center',
												paddingRight: '5%',
												paddingLeft: '5%'
											}}
										>
											<TouchableOpacity
												onPress={() => {
													this.props.navigation.navigate('Register');
												}}
											>
												<Text
													style={{
														marginTop: '6%',
														fontWeight: 'bold',
														color: '#f9f8f8'
													}}
												>
													New to Sollida? Register!
												</Text>
											</TouchableOpacity>
										</Row>
									</React.Fragment>
								)}
							</View>
						</Content>
					</ImageBackground>
				</Container>
			</Root>
		);
	}
}

const styles = StyleSheet.create({
	viewAlign: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttons: {
		alignItems: 'center'
	}
});

const mapStateToProps = state => ({
	value: state.ExampleReducer.value
});

const mapActionToProps = {
	setValue
};

export default connect(
	mapStateToProps,
	mapActionToProps
)(Login);
