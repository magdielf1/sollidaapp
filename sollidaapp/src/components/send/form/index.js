import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Slider,
  TouchableOpacity
} from "react-native";
import { Container, Content, Row, Item, Input } from "native-base";
import { LinearGradient } from "expo";
import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";
import { connect } from "react-redux";
import { setValue } from "../../../utils/actions/example"; 

class SendForm extends React.Component {
  state = { text: "", value: "" };
  onSelect(index, value) {
    this.setState({
      text: `Selected index: ${index} , value: ${value}`
    });
  }
  render() {
    return (
      <Container>
        <ImageBackground
          source={require("../../../../assets/bg.png")}
          style={{ width: "100%", height: "100%" }}
        >
          <Content style={{ padding: 5 }}>
            <View style={styles.balanceView}>
              <Text style={styles.balance}>Total Balance:</Text>
              <Text style={styles.balanceValue}>₽14,999.90</Text>
            </View>
            <Row>
              <Text style={styles.title}>ACCOUNT NUMBER:</Text>
            </Row>
            <Row>
              {/* <LinearGradient
					colors={['#00FFFF', '#17C8FF', '#329BFF', '#4C64FF', '#6536FF', '#8000FF']}
					start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
					style={{ height: 48, width: '100%', alignItems: 'center', justifyContent: 'center', padding: 2}}
				>
					<Input 
						style={{height: 48, width: '100%', backgroundColor: 'white'}} 
						placeholder="+715284912610" 
						placeholderTextColor="grey"
					/>
				</LinearGradient> */}
              <Content>
                <Item
                  regular
                  style={{ borderColor: "#51b3e5", borderRadius: 5 }}
                >
                  <Input
                    style={{ color: "white" }}
                    placeholderTextColor="rgba(255,255,255,0.6)"
                    placeholder="+715284912610"
                  />
                </Item>
              </Content>
            </Row>
            <Row>
              <Text style={styles.title}>AMOUNT:</Text>
            </Row>
            <Row style={{ justifyContent: "center", marginBottom: 10 }}>
              <Content>
                <Item
                  regular
                  style={{ borderColor: "#51b3e5", borderRadius: 5 }}
                >
                  <Input
                    style={{ color: "white" }}
                    placeholderTextColor="rgba(255,255,255,0.6)"
                    placeholder="₽1,000"
                  />
                </Item>
                <Text style={styles.textTransfer}>Transfer fee: ₽10</Text>
              </Content>
            </Row>
            <View style={{ flexDirection: "column", alignItems: "center" }}>
              <View style={{ width: "90%" }}>
                <Row>
                  <Content>
                    <Slider
                      minimumValue={1}
                      maximumValue={1000000}
                      minimumTrackTintColor={"#41b1ea"}
                      maximumTrackTintColor={"#51b3e5"}
                      thumbTintColor={"#51b3e5"}
                      onValueChange={data => {
                        console.log(data);
                      }}
                      style={{ width: "100%" }}
                    />
                  </Content>
                </Row>
                <Row style={{ justifyContent: "space-between" }}>
                  <Text style={styles.minimumSlider}>1</Text>
                  <Text style={styles.maximumSlider}>1000000</Text>
                </Row>
              </View>
            </View>
            <Row>
              <Text style={styles.title}>PAYMENT METHOD:</Text>
            </Row>
            <Row>
              <RadioGroup
                color="#41b1ea"
                onSelect={(index, value) => this.onSelect(index, value)}
              >
                <RadioButton value={"item1"}>
                  <Text style={styles.textRadio}>
                    {" "}
                    <FontAwesome
                      style={{ color: "white" }}
                      name="cc-visa"
                      size={20}
                    />
                    {"  "}
                    ****1111
                  </Text>
                </RadioButton>
                <RadioButton value={"item1"}>
                  <Text style={styles.textRadio}>
                    {" "}
                    <FontAwesome
                      style={{ color: "white" }}
                      name="cc-mastercard"
                      size={20}
                    />
                    {"  "}
                    ****4444
                  </Text>
                </RadioButton>
              </RadioGroup>
            </Row>
            <Row style={{ marginLeft: 11, marginTop: 8 }}>
              <Feather style={{ color: "white" }} name="plus" size={20} />
              <Text style={styles.textRadio}>
                {"  "}
                Add new card
              </Text>
            </Row>
            <Row style={{ justifyContent: "center", marginVertical: 25 }}>
              <TouchableOpacity
                onPress={() => false}
                style={{
                  borderWidth: 1,
                  borderColor: "rgba(0,0,0,0.1)",
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor: "#51b3e5",
                  borderRadius: 15,
                  padding: 10,
                  paddingHorizontal: 20
                }}
              >
                <Text style={styles.textButton}>Continue</Text>
              </TouchableOpacity>
            </Row>
          </Content>
        </ImageBackground>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  balanceView: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 15,
    marginTop: 10
  },
  balance: {
    color: "white",
    fontSize: 15,
    fontWeight: "500"
  },
  balanceValue: {
    color: "white",
    fontSize: 22,
    fontWeight: "600"
  },
  w100: {
    width: "100%"
  },
  title: {
    color: "white",
    marginBottom: 3,
    marginTop: 10
  },
  textTransfer: {
    color: "rgba(255,255,255,0.8)",
    fontSize: 10
  },
  minimumSlider: {
    color: "white"
  },
  maximumSlider: {
    color: "white"
  },
  textRadio: {
    color: "white"
  },
  textButton: {
    color: "white"
  }
});

const mapStateToProps = state => ({
  value: state.ExampleReducer.value
});

const mapActionToProps = {
  setValue
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(SendForm);
