import React from "react";
import {
  StyleSheet,
  Text,
  ImageBackground,
} from "react-native";
import { Container, Content, Row, Item, Input } from "native-base";
import { connect } from "react-redux";
import { setValue } from "../../../utils/actions/example";

class SendConfirm extends React.Component {
  render() {
    return (
      <Container>
        <ImageBackground
          source={require("../../../../assets/bg.png")}
          style={{ width: "100%", height: "100%" }}
        >
          <Content style={{ padding: 5 }}>
           <Row>
               <Text>Hello World! :D</Text>
           </Row>
          </Content>
        </ImageBackground>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
    value: state.ExampleReducer.value
  });
  
  const mapActionToProps = {
    setValue
  };
  
  export default connect(
    mapStateToProps,
    mapActionToProps
  )(SendConfirm);
  

