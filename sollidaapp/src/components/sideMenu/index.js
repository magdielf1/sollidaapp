import React, { Component } from "react";
import { StyleSheet, StatusBar } from "react-native";
import { Container, Text, Content, Row } from "native-base";

class SideMenu extends Component {
  render() {
    return (
      <Container style={styles.main}>
        <StatusBar hidden={this.props.navigation.state.isDrawerOpen} />
        <Content>
          <Row style={styles.sollidaRow}>
              <Text style={styles.sollidaText}>
                    SOLLIDA
              </Text>
              <React.Fragment>
               
              </React.Fragment>
          </Row>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "#26245de6",
    padding: 15
  },
  sollidaRow:{
    justifyContent: 'space-between'
  },
  sollidaText:{
    color: 'white',
    fontWeight: '500',
    fontSize: 20
  }
});

export default SideMenu;
