import React from 'react';

import FontAwesome from 'react-native-vector-icons/Feather';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

export default class TabIcon extends React.Component {
    render() {
        var color = 'red';

        if (this.props.type === 1) color = this.props.color;
        else color = this.props.focused ? '#7d7a7a' : '#b3b3b3';
        return (
            <React.Fragment>
                {this.props.font ? (
                    <FontAwesome
                        style={{ color: color }}
                        name={this.props.iconName || 'circle'}
                        size={25}
                    />
                ) : (
                        <SimpleLineIcons
                            style={{ color: color }}
                            name={this.props.iconName || 'circle'}
                            size={25}
                        />
                    )}
            </React.Fragment>
        );
    }
}