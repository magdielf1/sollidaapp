import React from 'react';
import { StyleSheet, Text, View, ImageBackground } from 'react-native';
import {
	Container,
	Header,
	Content,
	Accordion,
	H1,
	Row,
	Grid,
	Col,
	Thumbnail,
	Button
} from 'native-base';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';

import { LinearGradient } from 'expo';

export default class SubHeader extends React.Component {
	render() {
		return (
			<React.Fragment>
				<Container style={{ alignItems: 'center' }}>
					<ImageBackground
						source={require('../../../assets/bg.png')}
						style={{ width: '100%', height: 200 }}
					>
						<View style={styles.viewAlign}>
							<Thumbnail
								square
								small
								style={{ marginTop: 10 }}
								source={require('../../../assets/fav2.png')}
							/>

							<Row style={{ marginTop: 7, marginBottom: 10 }}>
								<Entypo
									style={{ color: 'white', top: 1, left: -20 }}
									name="list"
									size={35}
								/>
								<Text style={{ fontSize: 27, color: 'white' }}>
									0.000000 SLD
								</Text>
							</Row>

							<Row style={{ marginTop: 20, marginBottom: 35, width: '100%' }}>
								<Col>
									<Button
										info
										style={{
											margin: 10,
											width: '90%',
											height: 65,
											top: -35
										}}
									>
										<Text
											style={{
												fontSize: 20,
												color: 'white',
												width: '100%',
												textAlign: 'center'
											}}
										>
											<Feather
												style={{ color: 'white', top: 3 }}
												name="download"
												size={35}
											/>{' '}
											Requests{' '}
										</Text>
									</Button>
								</Col>
								<Col>
									<Button
										info
										style={{
											margin: 10,
											width: '90%',
											height: 65,
											top: -35
										}}
									>
										<Text
											style={{
												fontSize: 20,
												color: 'white',
												width: '100%',
												textAlign: 'center'
											}}
										>
											<Feather
												style={{ color: 'white', top: 3 }}
												name="upload"
												size={35}
											/>{' '}
											Send{' '}
										</Text>
									</Button>
								</Col>
							</Row>
						</View>
					</ImageBackground>
				</Container>
			</React.Fragment>
		);
	}
}

const styles = StyleSheet.create({
	viewAlign: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	}
});
