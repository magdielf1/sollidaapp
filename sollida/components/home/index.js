import React from 'react';
import SubHeader from './components/subHeader';
import HistoryHome from './components/historyHome';
export default class HomeRender extends React.Component {
	render() {
		return (
			<React.Fragment>
				<SubHeader />
				<HistoryHome />
			</React.Fragment>
		);
	}
}
