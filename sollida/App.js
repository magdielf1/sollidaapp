import React from 'react';
import {
	Router,
	Scene,
	Tabs,
	Actions,
	Stack,
	Drawer
} from 'react-native-router-flux';
import FontAwesome from 'react-native-vector-icons/Feather';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { StyleSheet, Text, View } from 'react-native';
import HomeRender from './components/home';

class TabIcon extends React.Component {
	render() {
		var color = 'red';

		if (this.props.type === 1) color = this.props.color;
		else color = this.props.focused ? '#7d7a7a' : '#b3b3b3';
		return (
			<React.Fragment>
				{this.props.font ? (
					<FontAwesome
						style={{ color: color }}
						name={this.props.iconName || 'circle'}
						size={25}
					/>
				) : (
					<SimpleLineIcons
						style={{ color: color }}
						name={this.props.iconName || 'circle'}
						size={25}
					/>
				)}
			</React.Fragment>
		);
	}
}

export default class App extends React.Component {
	render() {
		return (
			<Router>
				<Scene key="root" hideNavBar>
					<Tabs
						key="tabbar"
						tabs
						tabBarPosition="bottom"
						inactiveTintColor="#b3b3b3"
						activeTintColor="#7d7a7a"
						navigationBarStyle={styles.navBarModelA}
						titleStyle={styles.titleModelA}
						renderRightButton={() => (
							<TabIcon iconName="menu" type={1} font={false} color="#51b3e5" />
						)}
						renderLeftButton={() => (
							<TabIcon
								iconName="arrow-down"
								type={1}
								font={false}
								color="#51b3e5"
							/>
						)}
						hideNavBar
					>
						<Scene
							key="wallets"
							component={HomeRender}
							title="SOLLIDA"
							iconName="wallet"
							type={0}
							font={false}
							icon={TabIcon}
							onPress={() => Actions.wallets()}
							initial
						/>
						<Scene
							key="requests"
							component={HomeRender}
							title="REQUESTS"
							iconName="download"
							type={0}
							font={true}
							icon={TabIcon}
							onPress={() => Actions.requests()}
						/>
						<Scene
							key="send"
							component={HomeRender}
							title="SEND"
							iconName="upload"
							type={0}
							font={true}
							icon={TabIcon}
							onPress={() => Actions.send()}
						/>
						<Scene
							key="exchange"
							component={HomeRender}
							title="EXCHANGE"
							iconName="repeat"
							type={0}
							font={true}
							icon={TabIcon}
							onPress={() => Actions.send()}
						/>
					</Tabs>
				</Scene>
			</Router>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center'
	},
	iconPosition: {
		flex: 1,
		flexDirection: 'column',
		alignItems: 'center',
		alignSelf: 'center',
		justifyContent: 'center'
	},
	navBarModelA: {
		backgroundColor: '#26245de6',
		borderBottomColor: 'transparent'
	},
	titleModelA: {
		color: 'white'
	}
});
